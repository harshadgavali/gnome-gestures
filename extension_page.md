Touchpad Gesture improvements

This extension adds following gestures.

1. Switch windows from current workspace using 3-finger horizontal swipe.
2. Cyclic gestures between Desktop-Overview-AppGrid using 3 or 4 finger vertical swipe.
3. Override 3-finger gesture with 4-finger for switching workspace.
4. Switch app-pages using 3-finger swipe gesture on AppGrid.

* This also adds option to configure speed of gestures. (1.0 is default)


# Bugs
Report bugs on gitlab.
