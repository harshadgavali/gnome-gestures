Moved to https://github.com/harshadgavali/gnome-gesture-improvements .


This extension adds or modifies existing touchpad gestures on GNOME.
### Supported Versions
* GNOME Shell 40

## Installation
### From GNOME extensions website 
[extension#4245](https://extensions.gnome.org/extension/4245/gesture-improvements/).
### From git repo.
* Install extension.
```
git clone https://gitlab.gnome.org/harshadgavali/gnome-gestures.git/
cd gnome-gestures 
# git checkout origin/dev
make && make install
```
* Log out and log back in OR just restart.
* Enable extension via extensions app or via command line
```
gnome-extensions enable gestureImprovements@gestures
```

## Gestures available (including built-in ones)
|Mode                    |Fingers |Direction    | Action                               | Note                  |
| :---                   | ---:   | :---        | :---                                 | ---:                  |
|Desktop (Session)       |3       |Horizontal   |Switch windows from current workspaces| Not built-in          |
|Desktop (Session)       |4       |Horizontal   |Switch workspaces                     | Default is 3-fingers  |
|                        |3/4     |Vertical-Up  |Show Overview                         | Default is 3-fingers  |
|                        |3/4     |Vertical-Down|Show Application-Grid                 | Not built-in          |
|Overview (Window-Picker)|2/3/4   |Horizontal   |Switch workspaces                     | Default is 2/3-fingers|
|                        |3/4     |Vertical-Up  |Show Application-Grid                 | Default is 3-fingers  |
|                        |3/4     |Vertical-Down|Hide Overview                         | Default is 3-fingers  |
|Application-Grid        |2/3     |Horizontal   |Switch app pages                      | Default is 2-fingers  |
|                        |4       |Horizontal   |Switch workspaces                     | Default is 3-fingers  |
|                        |3/4     |Vertical-Up  |Hide Overview                         | Not built-in          | 
|                        |3/4     |Vertical-Down|Show Overvieww                        | Default is 3-fingers  |

## Customization
* For switching to windows from all workspaces using 3-fingers swipe, run 
```
gsettings set org.gnome.shell.window-switcher current-workspace-only false
```

* Add delay to alt-tab gesture, to ensure second windows get's selected, when fast swipe is done.
* Add sensitivity of swipe (touchpad swipe speed)
