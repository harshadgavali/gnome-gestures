
build: clean
	# gnome-extensions pack --force --extra-source=src --extra-source=constants.js --extra-source=ui --out-dir _build
	glib-compile-schemas ./schemas
	zip -r gestureImprovements@gestures.zip * \
		-i metadata.json extension.js prefs.js \
		-i constants.js schemas/gschemas.compiled \
		-i 'src/**' 'ui/**'
	mkdir _build
	mv gestureImprovements@gestures.zip _build/

clean:
	if test  -d _build; then rm -r _build; fi

install:
	gnome-extensions install -f ./_build/gestureImprovements@gestures.zip