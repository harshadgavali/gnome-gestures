/* exported TouchpadSwipeGesture */
'use strict';

const { Clutter, GObject, Shell } = imports.gi;

const Main = imports.ui.main;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const { TouchpadConstants } = Me.imports.constants;

const TouchpadState = {
	NONE: 0,
	PENDING: 1,
	HANDLING: 2,
	IGNORED: 3,
};

var TouchpadSwipeGesture = GObject.registerClass({
	Properties: {
		'enabled': GObject.ParamSpec.boolean(
			'enabled', 'enabled', 'enabled',
			GObject.ParamFlags.READWRITE,
			true),
		'orientation': GObject.ParamSpec.enum(
			'orientation', 'orientation', 'orientation',
			GObject.ParamFlags.READWRITE,
			Clutter.Orientation, Clutter.Orientation.HORIZONTAL),
	},
	Signals: {
		'begin': { param_types: [GObject.TYPE_UINT, GObject.TYPE_DOUBLE, GObject.TYPE_DOUBLE] },
		'update': { param_types: [GObject.TYPE_UINT, GObject.TYPE_DOUBLE, GObject.TYPE_DOUBLE] },
		'end': { param_types: [GObject.TYPE_UINT, GObject.TYPE_DOUBLE] },
	},
}, class TouchpadSwipeGesture extends GObject.Object {
	_init(
		nfingers, 
		allowedModes, 
		orientation, 
		followNaturalScroll = true, 
		checkAllowedGesture = null,
		gestureSpeed = 1.0) 
	{
		super._init();
		this._nfingers = nfingers;
		this._allowedModes = allowedModes;
		this.orientation = orientation;
		this._state = TouchpadState.NONE;
		this._checkAllowedGesture = checkAllowedGesture;
		this._cumulativeX = 0;
		this._cumulativeY = 0;
		this._followNaturalScroll = followNaturalScroll;
		this._stageCaptureEvent = global.stage.connect('captured-event::touchpad', this._handleEvent.bind(this));

		this.TOUCHPAD_BASE_HEIGHT = TouchpadConstants.TOUCHPAD_BASE_HEIGHT;
		this.TOUCHPAD_BASE_WIDTH = TouchpadConstants.TOUCHPAD_BASE_WIDTH;
		this.DRAG_THRESHOLD_DISTANCE = TouchpadConstants.DRAG_THRESHOLD_DISTANCE;
		this.SWIPE_MULTIPLIER = TouchpadConstants.SWIPE_MULTIPLIER * (typeof(gestureSpeed) !== 'number' ? 1.0 : gestureSpeed);
	}

	_handleEvent(_actor, event) {
		// log(`swipe multiplier = ${this.SWIPE_MULTIPLIER}`);
		if (event.type() !== Clutter.EventType.TOUCHPAD_SWIPE)
			return Clutter.EVENT_PROPAGATE;

		if (!this.enabled)
			return Clutter.EVENT_PROPAGATE;

		if (event.get_gesture_phase() === Clutter.TouchpadGesturePhase.BEGIN)
			this._state = TouchpadState.NONE;

		if (this._state === TouchpadState.IGNORED)
			return Clutter.EVENT_PROPAGATE;

		if (!this._nfingers.includes(event.get_touchpad_gesture_finger_count()))
			return Clutter.EVENT_PROPAGATE;

		if (this._state === TouchpadState.NONE && this._checkAllowedGesture !== null) {
			try {
				if ( this._checkAllowedGesture(event) !== true) {
					return Clutter.EVENT_PROPAGATE;
				}	
			} 
			catch (ex) {
				return Clutter.EVENT_PROPAGATE;
			}
		}

		if ((this._allowedModes !== Shell.ActionMode.ALL) && ((this._allowedModes & Main.actionMode) === 0))
			return Clutter.EVENT_PROPAGATE;

		let time = event.get_time();

		const [x, y] = event.get_coords();
		let [dx, dy] = event.get_gesture_motion_delta();

		if (this._state === TouchpadState.NONE) {
			if (dx === 0 && dy === 0)
				return Clutter.EVENT_PROPAGATE;

			this._cumulativeX = 0;
			this._cumulativeY = 0;
			this._state = TouchpadState.PENDING;
		}

		if (this._state === TouchpadState.PENDING) {
			this._cumulativeX += dx * this.SWIPE_MULTIPLIER;
			this._cumulativeY += dy * this.SWIPE_MULTIPLIER;

			const cdx = this._cumulativeX;
			const cdy = this._cumulativeY;
			const distance = Math.sqrt(cdx * cdx + cdy * cdy);

			if (distance >= this.DRAG_THRESHOLD_DISTANCE) {
				const gestureOrientation = Math.abs(cdx) > Math.abs(cdy)
					? Clutter.Orientation.HORIZONTAL
					: Clutter.Orientation.VERTICAL;

				this._cumulativeX = 0;
				this._cumulativeY = 0;

				if (gestureOrientation === this.orientation) {
					this._state = TouchpadState.HANDLING;
					this.emit('begin', time, x, y);
				} else {
					this._state = TouchpadState.IGNORED;
					return Clutter.EVENT_PROPAGATE;
				}
			} else {
				return Clutter.EVENT_PROPAGATE;
			}
		}

		const vertical = this.orientation === Clutter.Orientation.VERTICAL;
		let delta = (vertical ? dy : dx) * this.SWIPE_MULTIPLIER;
		const distance = vertical ? this.TOUCHPAD_BASE_HEIGHT : this.TOUCHPAD_BASE_WIDTH;

		switch (event.get_gesture_phase()) {
		case Clutter.TouchpadGesturePhase.BEGIN:
		case Clutter.TouchpadGesturePhase.UPDATE:
			if (this._followNaturalScroll)
				delta = -delta;

			this.emit('update', time, delta, distance);
			break;

		case Clutter.TouchpadGesturePhase.END:
		case Clutter.TouchpadGesturePhase.CANCEL:
			this.emit('end', time, distance);
			this._state = TouchpadState.NONE;
			break;
		}

		return this._state === TouchpadState.HANDLING
			? Clutter.EVENT_STOP
			: Clutter.EVENT_PROPAGATE;
	}

	destroy() {
		if (this._stageCaptureEvent) {
			global.stage.disconnect(this._stageCaptureEvent);
			delete this._stageCaptureEvent;
		}
	}
});