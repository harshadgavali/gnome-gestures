/* exported GestureExtension */
'use strict';

const { GObject, Shell } = imports.gi;
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const { TouchpadSwipeGesture } = Me.imports.src.swipeTracker;
const { OverviewControlsState, Settings } = Me.imports.constants;

var GestureExtension = class GestureExtension {
	constructor() {
		this._stateAdjustment = Main.overview._overview._controls._stateAdjustment;
		this._swipeTrackers = [
			{
				'swipeTracker': Main.wm._workspaceAnimation._swipeTracker,
				'nfingers': (Settings.DEFAULT_SESSION_WORKSPACE_GESTURE ? [3] : [4]),
				'disableOldGesture': true,
				'followNaturalScroll': true,
				'modes': Shell.ActionMode.NORMAL,
				'checkAllowedGesture': null,
				'gestureSpeed': 1/1.5
			},
			{
				'swipeTracker': Main.overview._overview._controls._workspacesDisplay._swipeTracker,
				'nfingers': [3, 4],
				'disableOldGesture': true,
				'followNaturalScroll': true,
				'modes': Shell.ActionMode.OVERVIEW,
				'checkAllowedGesture': (event) => {
					if (Main.overview._overview._controls._searchController.searchActive) {
						return false;
					}
					if (event.get_touchpad_gesture_finger_count() === 4) {
						return true;
					} else {
						return this._stateAdjustment.value === OverviewControlsState.WINDOW_PICKER;
					}
				},
				'gestureSpeed': 1/1.5
			},
			{
				'swipeTracker': Main.overview._overview.controls._appDisplay._swipeTracker,
				'nfingers': [3],
				'disableOldGesture': true,
				'followNaturalScroll': true,
				'modes': Shell.ActionMode.OVERVIEW,
				'checkAllowedGesture': () => {
					if (Main.overview._overview._controls._searchController.searchActive) {
						return false;
					}
					return this._stateAdjustment.value === OverviewControlsState.APP_GRID;
				}
			}
		];
	}

	enable() {
		this._swipeTrackers.forEach(entry => {
			let { 
				swipeTracker, 
				nfingers, 
				disableOldGesture, 
				followNaturalScroll, 
				modes, 
				checkAllowedGesture,
				gestureSpeed
			} = entry;
			if (typeof(gestureSpeed) !== 'number') {
				gestureSpeed = 1.0;
			}
			let touchpadGesture = new TouchpadSwipeGesture(
				nfingers,
				modes,
				swipeTracker.orientation,
				followNaturalScroll,
				checkAllowedGesture,
				gestureSpeed);

			this._attachGestureToTracker(swipeTracker, touchpadGesture, disableOldGesture);
		});
	}

	disable() {
		this._swipeTrackers.forEach(entry => {
			let { swipeTracker, disableOldGesture } = entry;
			swipeTracker._touchpadGesture.destroy();
			delete swipeTracker._touchpadGesture;

			swipeTracker._touchpadGesture = swipeTracker.__oldTouchpadGesture;
			if (disableOldGesture) {
				swipeTracker._touchpadGesture._stageCaptureEvent =
					global.stage.connect(
						'captured-event::touchpad',
						swipeTracker._touchpadGesture._handleEvent.bind(swipeTracker._touchpadGesture)
					);
			}
		});
	}

	_attachGestureToTracker(swipeTracker, touchpadSwipeGesture, disablePrevious) {
		if (swipeTracker._touchpadGesture) {
			if (disablePrevious && swipeTracker._touchpadGesture._stageCaptureEvent) {
				global.stage.disconnect(swipeTracker._touchpadGesture._stageCaptureEvent);
				delete swipeTracker._touchpadGesture._stageCaptureEvent;
			}
			swipeTracker.__oldTouchpadGesture = swipeTracker._touchpadGesture;
		}
		swipeTracker._touchpadGesture = touchpadSwipeGesture;
		swipeTracker._touchpadGesture.connect('begin', swipeTracker._beginGesture.bind(swipeTracker));
		swipeTracker._touchpadGesture.connect('update', swipeTracker._updateGesture.bind(swipeTracker));
		swipeTracker._touchpadGesture.connect('end', swipeTracker._endTouchpadGesture.bind(swipeTracker));
		swipeTracker.bind_property('enabled', swipeTracker._touchpadGesture, 'enabled', 0);
		swipeTracker.bind_property('orientation', swipeTracker._touchpadGesture, 'orientation',
			GObject.BindingFlags.SYNC_CREATE);
	}
};