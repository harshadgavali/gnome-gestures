/* exported init */
'use strict';

const { GLib } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const Constants = Me.imports.constants;
const { GestureExtension } = Me.imports.src.gestures;
const { AltTabGestureExtension } = Me.imports.src.altTab;
const { OverviewRoundTripGestureExtension } = Me.imports.src.overviewRoundTrip;

class Extension {
	constructor() {
		this._extensions = [];
		this.settings = null;
		this._settingChangedId = null;
		this._reloadWaitId = 0;
	}

	enable() {
		this.settings = ExtensionUtils.getSettings();
		this._settingChangedId = this.settings.connect('changed', this.reload.bind(this));
		this._enable();
	}

	disable() {
		this.settings.disconnect(this._settingChangedId);
		this._disable();

		if (this._reloadWaitId !== 0) {
			GLib.source_remove(this._reloadWaitId);
			this._reloadWaitId = 0;
		}
	}

	reload(_settings, key) {
		if (this._reloadWaitId !== 0) {
			GLib.source_remove(this._reloadWaitId);
			this._reloadWaitId = 0;
		}

		else {
			this._reloadWaitId = GLib.timeout_add(
				GLib.PRIORITY_DEFAULT,
				(key == 'default-session-workspace' ? 0 : Constants.RELOAD_DELAY),
				() => {
					this._disable();
					this._enable();
					this._reloadWaitId = 0;
					return GLib.SOURCE_REMOVE;
				}
			);
		}
	}

	_enable() {
		this._initializeSettings();
		this._extensions = [
			new GestureExtension(),
			new AltTabGestureExtension(),
			new OverviewRoundTripGestureExtension()
		];
		this._extensions.forEach(extension => extension.enable());
	}

	_disable() {
		for (let i = 0; i < this._extensions.length; ++i) {
			this._extensions[i].disable();
			delete this._extensions[i];
		}
		this._extensions = [];
	}

	_initializeSettings() {
		this._updateTouchpadScale();
		this._updateAltTabDelay();
		
		Constants.Settings.DEFAULT_SESSION_WORKSPACE_GESTURE = this.settings.get_boolean('default-session-workspace');
	}

	_updateTouchpadScale() {
		Constants.TouchpadConstants.SWIPE_MULTIPLIER =
			Constants.TouchpadConstants.DEFAULT_SWIPE_MULTIPLIER *
			this.settings.get_double('touchpad-speed-scale');
	}

	_updateAltTabDelay() {
		Constants.AltTabConstants.DELAY_DURATION = this.settings.get_int('alttab-delay');
	}
}

function init() {
	return new Extension();
}
